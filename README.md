# SetSDK Demo App Using MVVM + RxSwift

This repository contains sample code for a SetSDK project using the MVVM architecture.

You can read the corresponding blog post at https://medium.com/p/5a37d027950f.
