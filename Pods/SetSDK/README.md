![Logo](https://s3.amazonaws.com/set-static-assets/logos/readme.png)

SetSDK is a mobile extension which allows on-device learning and forecasting of user behaviors in the physical world. Models are trained and stored directly on the user's device, making them real-time and available when offline. The first release of the SDK is all about locations, where the user is and where they are going next.

# Communication

- If you **need help**, shoot us a message at support@set.gl.
- If **found a bug**, or **have a feature request**, add an Issue on the [SetSDK Repo](https://gitlab.com/weareset/SetSDK/issues)

# Getting Started

### Requirements

- iOS 8.0+
- Xcode 8.0

### Installation with CocoaPods

```ruby
pod 'SetSDK'
```

### Request Client Credentials

Email <sander@set.gl> and we'll send you an invite with a client id/secret pair.

### Setup your app to receive location and motion data

- Enable "Location updates" in your app's Background Modes
- Provide a Info.plist message to ask the user for location data
- Provide a Info.plist message to ask the user for motion data

### Launch

```swift
let configuration = SetConfiguration(clientId: clientId, clientSecret: clientSecret, userId: userId)
SetSDK.instance.launch(withConfiguration: configuration, completion: { error in
  if let error = error {
    // Oops, error
  } else {
    // SetSDK is ready and learning!
  }
})
```

See [SetSDK.launch(withConfiguration:completion:)](#setsdklaunchwithconfigurationcompletion) for more.

# Ways to use the SetSDK now

Here are a few examples how you can make use of the API today:

1. Use Set to handle always-on location updates without battery drain
2. Automated geofencing of user locations (no manual entry)
3. Subscribe to notifications of a user arriving and departing locations (no manual entry)
4. When the user is about to depart on a trip, predict where the destination will be (see [getDestination](#setsdkgetdestinationfromlocationfromtimestamp))

For beta testers, you can start making use of a few more of the predictive methods right away:

1. Learn named locations (e.g. Home) without user needing to manually enter information (see [getNamedPlace](#setsdkgetnamedplace))
2. At any moment in time, learn the users next most likely place or behavior (see [getNext](#setsdkgetnext))


## Usage Example

```swift
SetSDK.instance.onDeparture(.home) { departure in
  if let destinations = SetSDK.instance.getDestination(fromLocation: departure.location, fromTimestamp: departure.date), let destination = destinations.first {
    let label = destination.label ?? "your destination"
    let arrivalTime = destination.bounds.rawBounds.end
    let minutesUntilArrival = Int(Date().timeIntervalSince(arrivalTime) / 60)
}
```

# API Methods

### SetSDK.launch(withConfiguration: completion)

The launch method allows you to start the SetSDK at any time within your app. We recommend you start it as part of the initialization sequence of your application, this will allow the SDK to do persistent behavior learning, allowing the APIs and notifications to continuously improve.

You will need to provide the launch method with a ```SetConfiguration``` which will include your ```clientId``` and ```clientSecret```. If you don't have your id/secret pair yet, email <sander@set.gl> for an invite. The ```userId``` can be any unique identifier for your user, i.e., phone number, email address, or some other internal ID you manage.

```swift
let configuration = SetConfiguration(clientId: clientId, clientSecret: clientSecret, userId: userId)
SetSDK.instance.launch(withConfiguration: configuration, completion: { error in
  if let error = error {
    // Oops, error
  } else {
    // SetSDK is ready and learning!
  }
})
```

### SetSDK.shutDown()

To stop the SetSDK from learning, use the ```shutDown``` method.

```swift
SetSDK.instance.shutDown()
```

### SetSDK.onArrival(to: SetPlaceType)

Automatically registers listeners to arrivals at any significant place for the user. You do not need to pre-define geofences, as Set will build them from observation.

```swift
SetSDK.instance.onArrival(to: .any) { /* do onEvent */}
```

To send the user a welcome message every time they arrive Home, you would,

```swift
SetSDK.instance.onArrival(to: .home) {
    let content = UNMutableNotificationContent()
    content.body = "Welcome home!"
    let request = UNNotificationRequest(identifier: UUID().uuidString,
                                        content: content,
                                        trigger: nil)
    UNUserNotificationCenter.current().add(request)
}
```

### SetSDK.onDeparture(from: SetPlaceType)

Automatically registers listeners to departures from any significant place for the user. You do not need to pre-define geofences, as Set will build them from observation.

```swift
SetSDK.instance.onDeparture(from: .any) { /* do onEvent */}
```

### SetSDK.getDestination(fromLocation: CLLocationCoordinate2D, fromTimestamp: Date)

Given a ```CLLocation```, predicts where the user is most likely to go next if the trip is starting right now. This is useful for any interface where the user is prompted to enter their next destination. When available, labels will be returned with the destination. Based on established, or previously observed, places.

```swift
let destinations: [SetState] = SetSDK.instance.getDestination(fromLocation: lastPlace, fromTimestamp: lastTime)
```

# API Types

### SetState

```swift
public enum SetState {
  case unknown
  case place(place: SetPlace, bounds: SetBounds, confidence: Double)
  case trip(trip: SetTrip, bounds: SetBounds, confidence: Double)
}
```

#### SetPlace

```swift
struct SetPlace {
  let id: String
  let location: CLLocationCoordinate2D
  let label: String?
}
```

### SetTrip

```swift
struct SetTrip {
  let id: String
  let start: SetPlace
  let startTime: Date
  let end: SetPlace
  let endTime: Date
  let mode: SetTripMode
}
```

### SetStateEvent

```swift
struct SetStateEvent {
  let id: String
  let location: CLLocationCoordinate2D
  let date: Date
  let eventType: SetEventType
  let label: String?
}
```

### SetPlaceType

```swift
enum SetPlaceType {
  case home
  case any
}
```

### SetTripMode

```swift
  enum SetTripMode {
    case unknown
    case walking
    case running
    case biking
    case automotive
  }
```

### SetEventType

```swift
enum SetEventType {
  case arrive
  case depart
}
```

# Beta API

We are interested in any app developer who wants to try out the API methods in our beta version. If you would like to test the beta, get in touch with us for the download. Let us know a bit about the app you are building and what your timeline is so that we can help get you the best info possible.

<andrew@set.gl>

### SetSDK.getNow()

Get the details about the user's current state, including where they are and how long they have left in that place.

```swift
let currentState: (state: SetState, timestamp: Date) = SetSDK.instance.getNow()
```

```SetState``` will contain the forecasted end time for the current state.

### SetSDK.getNext()

Get the forecast for the user's next most likely state. This takes into account the forecasted end time for the current state (as made available in [getNow](#setsdkgetnow)).

```swift
let nextState: (state: SetState, timestamp: Date) = SetSDK.instance.getNext()
```

### SetSDK.getAllNext()

Will return the next most likely states for the user as a ranked list, with the first being the most likely (and matching [getNext](#setsdkgetnext).

```swift
let allNextState: (states: [SetState], timestamp: Date) = SetSDK.instance.getAllNext()
```

### SetSDK.getNamedPlace(_:)

Learn the location and SetID of named places (e.g. Home). Combining this information with [getNext](#setsdkgetnext) will allow you to easily label predicted destinations in your user interface.

```swift
let home: SetPlace = SetSDK.instance.getNamedPlace(.home)
```

# Resources

* [Documentation](https://gitlab.com/weareset/SetSDK)
* [Website](https://www.set.gl/)
* [Blog](https://blog.set.gl/)
* Follow us on Twitter: [@everyset](https://twitter.com/everyset)

