import Foundation
import RxSwift
import Set
import CoreLocation

class SetViewModel {

  // The possible events our view controller and view model can trigger
  enum Event {
    case logOut
  }

  // Events for our parent view model to subscribe to
  let events = PublishSubject<Event>()

  // Wrap the SetSDK.onDeparture API in an Observable.
  // We can derive other Observables from it, and bind our views to those Observables.
  private let setStateEvents = Observable<SetStateEvent>.create { observer -> Disposable in
    SetSDK.instance.onDeparture(from: .any) { setStateEvent in
      observer.onNext(setStateEvent)
    }
    return Disposables.create()
  }
  .share()

  // Create an Observable of a departure time string by mapping setStateEvents to String
  lazy var departureTimeString: Observable<String> = self.setStateEvents
    .map { setStateEvent -> String in
      return "Departing at \(self.dateFormatter.string(from: setStateEvent.date)) from"
    }
    .startWith("Departing")

  // Create an Observable of a departure place name by mapping setStateEvents to String
  lazy var departurePlaceString: Observable<String> = self.setStateEvents
    .map { setStateEvent -> String in
      return setStateEvent.label ?? "Unknown"
    }
    .startWith("None")

  // Create an Observable of the destination SetPlace we can derive other Observables from
  private lazy var destinationPlace: Observable<SetPlace?> = self.setStateEvents
    .map { setStateEvent -> SetPlace? in
      if let destination = SetSDK.instance.getDestination(fromLocation: setStateEvent.location)?.first,
        case .place(let place, _, _) = destination {
        return place
      } else {
        return nil
      }
    }
    .share()

  // Create an Observable of destination place name by mapping the above destinationPlace Observable
  lazy var destinationPlaceString: Observable<String> = self.destinationPlace
    .map { setPlace -> String in
      return setPlace?.label ?? "Unknown"
    }
    .startWith("None")

  // Create an Observable of destination place location by mapping the above destinationPlace Observable
  lazy var destinationPlaceLocation: Observable<CLLocationCoordinate2D?> = self.destinationPlace
    .map { setPlace -> CLLocationCoordinate2D? in
      return setPlace?.location
    }

  // Create an Observable of the destination SetBounds we can derive other Observables from
  private lazy var destinationBounds: Observable<SetBounds?> = self.setStateEvents
    .map { setStateEvent -> SetBounds? in
      if let destination = SetSDK.instance.getDestination(fromLocation: setStateEvent.location)?.first,
        case .place(_, let bounds, _) = destination {
        return bounds
      } else {
        return nil
      }
    }
    .share()

  // Create an Observable of arrival time string by mapping the above destinationBounds Observable
  lazy var arrivalTimeString: Observable<String> = self.destinationBounds
    .map { setBounds -> String in
      if let setBounds = setBounds,
        case .future(let start, _) = setBounds {
        return "Arriving at \(self.dateFormatter.string(from: start.date)) to"
      } else {
        return "Arriving"
      }
    }
    .startWith("Arriving")

  private let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MMM d, h:mma"
    return dateFormatter
  }()

  // A function that will notify the parent view model of the user's intent to log out
  func logOut() {
    events.onNext(.logOut)
  }
}
