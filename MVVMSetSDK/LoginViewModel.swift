import Foundation
import RxSwift

class LoginViewModel {

  enum Event {
    case loggedIn(phoneNumber: String)
  }

  let events = PublishSubject<Event>()

  let phoneNumber = Variable<String?>(nil)

  lazy private(set) var phoneNumberIsValid: Observable<Bool> = self.phoneNumber
    .asObservable()
    .map { number in
      guard let number = number else { return false }
      let regex = try! NSRegularExpression(pattern: "^[0-9]{11}$")
      let matches = regex.matches(in: number, options: [], range: NSRange(location: 0, length: number.characters.count))
      return matches.count == 1
    }

  func submit() {
    guard let number = phoneNumber.value else { return }
    events.onNext(.loggedIn(phoneNumber: number))
  }

}
