import Foundation
import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
  var viewModel: LoginViewModel!

  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var button: UIButton!

  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "Log In"

    textField.rx.text
      .bindTo(viewModel.phoneNumber)
      .addDisposableTo(disposeBag)

    viewModel.phoneNumberIsValid
      .bindTo(button.rx.isEnabled)
      .addDisposableTo(disposeBag)

    button.rx.tap
      .bindNext(viewModel.submit)
      .addDisposableTo(disposeBag)
  }
}
