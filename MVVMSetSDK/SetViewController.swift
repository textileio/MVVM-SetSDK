import Foundation
import UIKit
import RxSwift
import MapKit

class SetViewController: UIViewController, MKMapViewDelegate {
  var viewModel: SetViewModel!

  @IBOutlet weak var departingTimeLabel: UILabel!
  @IBOutlet weak var departingPlaceLabel: UILabel!
  @IBOutlet weak var arrivalTimeLabel: UILabel!
  @IBOutlet weak var destinationPlaceLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!

  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "SetSDK"
    navigationItem.setHidesBackButton(true, animated: false)
    let logOutItem = UIBarButtonItem(title: "Log Out", style: .plain, target: nil, action: nil)
    navigationItem.setRightBarButton(logOutItem, animated: false)

    mapView.showsUserLocation = true
    mapView.delegate = self

    // Bind the UI to the view model's Observables

    // First, bind the Log Out bar button item to the view model's logOut function
    logOutItem.rx.tap
      .bindNext(viewModel.logOut)
      .addDisposableTo(disposeBag)

    // Then bind all the String Observables to UILabels
    viewModel.departureTimeString
      .bindTo(departingTimeLabel.rx.text)
      .addDisposableTo(disposeBag)

    viewModel.departurePlaceString
      .bindTo(departingPlaceLabel.rx.text)
      .addDisposableTo(disposeBag)

    viewModel.arrivalTimeString
      .bindTo(arrivalTimeLabel.rx.text)
      .addDisposableTo(disposeBag)

    viewModel.destinationPlaceString
      .bindTo(destinationPlaceLabel.rx.text)
      .addDisposableTo(disposeBag)

    // Remove any existing DestinationAnnotation from the map, and add a
    // DestinationAnnotation for any new destination place that is received
    viewModel.destinationPlaceLocation
      .subscribe(onNext: { [weak self] coordinate in
        guard let `self` = self else { return }
        self.mapView.removeAnnotations(self.mapView.annotations.filter { $0 is DestinationAnnotation })
        guard let coordinate = coordinate else { return }
        self.mapView.addAnnotation(DestinationAnnotation(coordinate: coordinate))
      })
      .addDisposableTo(disposeBag)
  }

  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard annotation is DestinationAnnotation else { return nil }
    return MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
  }

  func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
    // Adjust the map to show all the annotations (user location and any destination annotation)
    mapView.showAnnotations(self.mapView.annotations, animated: true)
  }

}

class DestinationAnnotation: NSObject, MKAnnotation {
  let coordinate: CLLocationCoordinate2D
  let title: String? = nil
  let subtitle: String? = nil

  init(coordinate: CLLocationCoordinate2D) {
    self.coordinate = coordinate
  }
}

